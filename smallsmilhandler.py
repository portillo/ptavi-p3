#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.etiquetas = []

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """

        if name in ['root-layout', 'region', 'img', 'audio', 'textstream']:
            temp_label = {'etiqueta': name}
            for attr_name in attrs.getNames():
                temp_label[attr_name] = attrs.get(attr_name)
            self.etiquetas.append(temp_label)

    def get_tags(self):
        return self.etiquetas


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    etiq = cHandler.get_tags()
    print(etiq)
