#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import json
from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler
import urllib.request


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    etiq = cHandler.get_tags()

    if len(sys.argv) != 2:
        print('Usage: python3 karaoke.py file.smil.')
    else:
        parser.parse(open(sys.argv[1], 'r'))
        ficherojson = sys.argv[1].replace('.smil', '.json')
        with open(ficherojson, 'w') as file:
            json.dump(etiq, file)

        for etiqueta in etiq:
            for atributo in etiqueta:
                if atributo != 'etiqueta':
                    print(atributo, '=', etiqueta[atributo], '\t', end="")
                    if atributo == 'src':
                        url = etiqueta[atributo]
                        local = url.split('/')[-1]
                        urllib.request.urlretrieve(url, local)
                        url = local
                        print(url)
                else:
                    print(etiqueta[atributo], '\t', end="")
            print('\n')
